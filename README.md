# LIBECS

`libecs` is a header-only C++ library “““designed””” to be a “““suitable”””
framework for working in the Entity-Component-System architecture. 

It has a somewhat friendly programmer interface, and supports rudimentary 
events; I'm still working on it, so I don't know what will become of it. 
Help isn't -- well, I don't _need_ help -- but if you have input I'd very 
much like to hear it!

## Requirements
 
You should only need the C++17 STL, a `std=c++17` compliant compiler and 
`make` which everyone should already have. To use it in your project, well
I don't have an application using it just yet so let's leave this as a TODO
and come back to it later.
