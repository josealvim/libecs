#include <ecs/world.hpp>
#include <ecs/event.hpp>

#include <string>
#include <iostream>

using namespace ecs;

struct Component: ComponentBase {
	Component(int val): value(val) {}
	int value;
};

int main () {
	using namespace ecs;

	World w;
	auto foo = w.New();
	foo[New(Component{.value = 10})];

	EventManager evm;
	auto intprinter = [](int const& i) {
		std::cout << "raised int: " << i << std::endl;
	};
	auto handle = evm.register_callback(intprinter);
	
	evm.raise(3);
	evm.dispatch();
	evm.raise(4);
	evm.dispatch();
	evm.deregister_callback(handle);
	evm.raise(5);
	evm.dispatch();
	return 0;
}
