#pragma once

#include <ecs/identity.hpp>

#include <type_traits>
#include <typeindex>
#include <map>
#include <memory>

namespace ecs {

struct ComponentBase {
	ComponentBase() = default;
	virtual ~ComponentBase() = default;
};

template <typename T>
struct Del {
	static_assert(std::is_base_of<ComponentBase, T>::value);
};

template <typename T>
struct Get {
	static_assert(std::is_base_of<ComponentBase, T>::value);
};

template <typename T>
struct New {
	using Component = T;
	New ( Component _t ) : t(std::move(_t)) {}

	Component t;
};

class CSet {
	public:
	bool has(std::type_index const& type) const {
		return data.find(type) != data.end();
	}

	template <typename T>
	bool has() const {
		return has(tID<T>);
	}

	template <typename T>
	T const& operator[] (Get<T>) const {
		auto &base = *data.at(tID<T>);
		return dynamic_cast <T const&> (base);
	}

	template <typename T>
	T& operator[] (Get<T>) {
		// Scott's magic trick
		auto const& t = std::as_const(*this)[Get<T>()];
		return const_cast<T&>(t);
	}

	template <typename T>
	T& operator[] (New<T>&& val) {
		data[tID<T>] = std::make_unique<T>(std::move(val.t));
		return (*this)[Get<T>()];
	}

	template <typename T>
	void operator[] (Del<T>) {
		data.erase(tID<T>);
	}

	protected:
	std::map<std::type_index, std::unique_ptr<ComponentBase>> data;
};

}
