#pragma once

#include <ecs/identity.hpp>
#include <ecs/entity.hpp>

namespace ecs {

class World: 
public EntityTypesManager, 
public EntityDataManager {
	public:
	EntityHandle New () {
		static ID_t   free;
		ID_t next   = free++;
	
		auto next_h = EntityHandle(*this, *this, next);
		
		construct_cset_for(next);
		notify_birth(next_h);

		return next_h;
	}

	void Del (EntityHandle& e) {
		notify_death(e);
		destroy_cset_for(e.id());
	}
};

}
