#pragma once

#include <typeindex>
#include <cstddef>

namespace ecs {

using ID_t = size_t;

template <typename T>
inline auto const tID = std::type_index(typeid(T));

}
