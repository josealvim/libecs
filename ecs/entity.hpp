#pragma once

#include <ecs/compset.hpp>
#include <ecs/identity.hpp>

#include <typeindex>
#include <algorithm>
#include <memory>
#include <set>
#include <map>

namespace ecs {

class EntityHandle;

using Entities      = std::set<EntityHandle>;
using RequiredTypes = std::set<std::type_index>;

class EntityDataManager {
	protected:
	EntityDataManager() = default;

	auto& construct_cset_for(ID_t uid) {
		return ents[uid] = CSet();	
	}
	
	void destroy_cset_for(ID_t uid) {
		ents.erase(uid);
	}

	public:
	CSet& data(ID_t uid) {
		return ents.at(uid);
	}

	CSet const& data(ID_t uid) const {
		return ents.at(uid);
	}

	protected:
	std::map<ID_t, CSet> ents;
};

class EntityTypesManager {
	protected:
	EntityTypesManager() = default;

	public:
	void maintain_cache_for(RequiredTypes&& these_types) {
		auto [it, inserted] = cached_buffers.try_emplace(
			std::move(these_types), Entities()
		);

		if (!inserted) {
			return;
		} else {
			auto const &[req, ents] = *it;
			rebuild_cache_for(req);
		}
	}

	void rebuild_cache_for (RequiredTypes const& req) {
		for (auto const& ent : all_entities) 
			add_to_buffer_if_fulfills_req(ent, req);
	}

	void add_to_buffer_if_fulfills_req (	
		EntityHandle const& ent, 
		RequiredTypes const& req, 
		Entities* buffer = nullptr
	);

	void notify_birth(EntityHandle const &ent) {
		all_entities.insert(ent);
	}

	void notify_death(EntityHandle const &ent) {
		all_entities.erase(ent);
	}

	void notify_addition(EntityHandle const& ent, std::type_index t) {
		auto is_in = 
		[](auto const& req, auto const& t)->bool {
			return req.find(t) != req.end();
		};

		for (auto &[req, buffer] : cached_buffers) {
			if (!is_in(req, t))
				continue;
			else {
				add_to_buffer_if_fulfills_req(ent, req, &buffer);
			}
		}
	}
	
	void notify_deletion(EntityHandle const& ent, std::type_index t) {
		auto is_in = 
		[](auto const& req, auto const& t)->bool {
			return req.find(t) != req.end();
		};

		for (auto &[req, buffer] : cached_buffers) {
			if (is_in(req, t))
				buffer.erase(ent);
			else {
				continue;
			}
		}
	}

	
	void notify_dirtying(EntityHandle const&, std::type_index) {
		// nothing clever to do with touching attributes
		return;
	}

	private:
	std::map<RequiredTypes, Entities> cached_buffers;
	Entities all_entities;
};	

class EntityHandle {
	public:	
	EntityHandle(
		EntityTypesManager& _tman, 
		EntityDataManager&  _dman, 
		ID_t id
	): tman(_tman), dman(_dman), uid(id) {}

	~EntityHandle() = default; 

	template <typename T>
	bool has() const {
		return has(tID<T>);
	}

	bool has(std::type_index const& type) const {
		return dman.data(uid).has(type);
	}

	template <typename T>
	T const& operator[] (Get<T>) const {
		return dman.data(uid)[Get<T>()];
	}

	template <typename T>
	T& operator[] (Get<T>) {
		tman.notify_dirtying(*this, tID<T>);
		return dman.data(uid)[Get<T>()];
	}

	template <typename T>
	T& operator[] (New<T>&& val) {
		tman.notify_addition(*this, tID<T>);
		return dman.data(uid)[std::forward<New<T>>(val)];
	}

	template <typename T>
	void operator[] (Del<T>) {
		tman.notify_deletion(*this, tID<T>);
		return dman.data(uid)[Del<T>()];
	}

	friend 
	bool operator< (EntityHandle const& l, EntityHandle const& r) {
		return l.uid < r.uid;
	}

	ID_t id() const { 
		return uid;
	}
	
	private:
	EntityTypesManager& tman; 
	EntityDataManager&  dman; 

	ID_t uid;
};

using Entity = EntityHandle;

inline 
void EntityTypesManager::add_to_buffer_if_fulfills_req (
	EntityHandle const& ent, 
	RequiredTypes const& req, 
	Entities* buffer
) {
	auto unmet_req_ptr = std::find_if(
		req.begin(), req.end(),
		[&](auto const& type) {
			return !ent.has(type);
		}
	);

	bool should_insert = unmet_req_ptr != req.end();

	if (!should_insert ) 
		return;
	
	if ( buffer == nullptr )
		buffer = &cached_buffers.at(req);
	
	buffer->insert(ent);
}

}
