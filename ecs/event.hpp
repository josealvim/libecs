#pragma once

#include <map>
#include <any>
#include <vector>
#include <memory>
#include <iterator>
#include <typeindex>
#include <algorithm>
#include <functional> 
#include <type_traits>

namespace ecs :: aux {

template <typename T>
class T_Switcher {
	public:
	T const & curr() const {
		switch (selector){
		case false:
			return first;
		case true:
			return second;
		}
	}

	T const & back() const {
		switch (!selector){
		case false:
			return first;
		case true:
			return second;
		}
	}

	T& curr() {
		return const_cast<T&>(
			std::as_const(*this).curr()
		);
	}
	
	T& back() {
		return const_cast<T&>(
			std::as_const(*this).back()
		);
	}

	void flip() {
		selector = !selector;
	}

	private:
	T first, second;
	bool selector;
};

}

namespace ecs {

template <typename T>
using Listener       = std::function<void(T const&)>;
using ListenerHandle = std::pair<std::type_index, size_t>;

class EventManager {
	private:
	using EventBuffer = std::vector<std::any>;
	using CallbackMap = std::map<ListenerHandle, std::any>;

	using EvBufferSwitch = aux::T_Switcher<EventBuffer>;
	using CalbkMapSwitch = aux::T_Switcher<CallbackMap>;

	using Applier = std::function<void(std::any&, std::any const&)>;
	
	public:
	EventManager():
		dispatching(false),
		mustcleanup(false) {}

	void raise(std::any ev) {
		auto index = std::type_index(ev.type());
		try {
			auto num_listeners_for_type = listenernum.at(index); 
			if ( num_listeners_for_type < 1 ) 
				return;
			else 
				buffer_sw.curr().push_back(ev);
		} catch (...) {}
	}

	template <typename T>
	ListenerHandle register_callback(Listener<T> fn) {
		static size_t free_cbk_id = 0;
		size_t callback_id = free_cbk_id++;

		listenernum[tID<T>] += 1;

		applicators.try_emplace(tID<T>, 
		[](std::any& listener, std::any const& ev) {
			auto       &F = std::any_cast<Listener<T>&>(listener);
			auto const &E = std::any_cast<T const&>(ev);
			F(E);
		});

		ListenerHandle handle = std::make_pair(tID<T>, callback_id);
		cbkmap_sw.curr()[handle] = fn;
		return handle;
	}

	template <typename T>
	ListenerHandle register_callback(T t) {
		return register_callback(std::function(std::move(t)));
	}

	void deregister_callback(ListenerHandle const& handle) {
		auto [index, id] = handle;
		
		cbkmap_sw.curr().erase(handle);
		listenernum[index] -= 1;
		
		if (dispatching)
			mustcleanup = true;
		else
			cleanup();
	}

	void dispatch() {
		// setting up double buffer goodness 
		auto &callbacks  = cbkmap_sw.curr();
		auto &eventbuff  = buffer_sw.curr();
		cbkmap_sw.back() = callbacks;
		buffer_sw.back() . clear();

		cbkmap_sw.flip();
		buffer_sw.flip();

		dispatching = true;

		// okay, now we start
		for (auto const &ev : eventbuff) {
			auto index  = std::type_index(ev.type());
	
			Applier call;
			try {
				call = applicators.at(index);
			} catch (...) {
				continue;
			}
		
			auto [from, to] = waiting_for_index(index, callbacks);

			for (auto it = from; it != to; it++) {
				auto [k, listener] = *it;
				call(listener, ev);
			}
		}

		dispatching = false;

		if (mustcleanup)
			cleanup();
	}

	private:
	void cleanup() {
		std::vector<std::type_index> dying;
		std::transform(
			listenernum.begin(),listenernum.end(),
			std::back_insert_iterator(dying),
			[](auto const& p) -> std::type_index {
				auto &[k, v] = p;
				return k;
			}
		);

		std::for_each(
			dying.begin(), dying.end(),
			[this](std::type_index const& index){
				listenernum.erase(index);
				applicators.erase(index);
			}
		);

		mustcleanup = false;
	}

	std::pair<
		CallbackMap::iterator,
		CallbackMap::iterator
	> waiting_for_index (
		std::type_index index, 
		CallbackMap& callbacks
	) {
		auto is_same_type_as_ev = [&]
		(auto const& kv)->bool{
			auto &[p, _] = kv;
			auto &[k, v] = p;
			return k == index;
		};

		auto start = std::find_if(
			callbacks.begin(),
			callbacks.end(),
			is_same_type_as_ev
		);

		auto finis = std::find_if_not(
			start,
			callbacks.end(),
			is_same_type_as_ev
		);

		return std::make_pair(start, finis); 
	}

	EvBufferSwitch buffer_sw;
	CalbkMapSwitch cbkmap_sw;

	std::map<std::type_index, Applier> applicators;
	std::map<std::type_index,  size_t> listenernum;

	bool dispatching;
	bool mustcleanup;
};

}
