CXX = g++ -std=c++17 -I. -O0 -g

ECS_HEADERS = $(shell find ecs/ -type f -name '*.hpp')

example.out: example.cpp $(ECS_HEADERS) 
	$(CXX) -o $@ $<
